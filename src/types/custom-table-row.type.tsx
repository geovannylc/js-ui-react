export type CustomTableRow = {
	align: "left" | "right" | "inherit" | "center" | "justify";
	data: {
		[index: string]: string | number | null | undefined;
	}
};
