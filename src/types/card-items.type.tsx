import { Currency } from "./currency.type";

export type CardItem = {
	code: string;
	symbol: string;
	rate: string;
	description: string;
	rate_float: number;
	currency: Currency;
	disclaimer: string;
	updated_at: string;
};
