import { CustomTableRow } from "./custom-table-row.type";

export type CustomTablePros = {
	columns: string[];
	rows: CustomTableRow[];
};
