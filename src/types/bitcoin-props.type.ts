import { CoinDesk } from "./coindesk.type";

export type BitcoinProps = {
	data?: CoinDesk | undefined | null;
};
