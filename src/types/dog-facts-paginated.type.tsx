export type DogFactsPaginated = {
	current_page: number;
	data: {
		id: number;
		fact: string;
	}[],
	first_page_url: string;
	from?: string;
	last_page: number;
	last_page_url: string;
	links: {
		url?: string;
		label: string;
		active: false
	} [],
	next_page_url?: string;
	path: string;
	per_page: 25,
	prev_page_url?: string;
	to?: string;
	total: number;
}
