/** @format */

import { useEffect, useState } from "react";
import { COIN_DESK_API_URL } from "../constants";

const fetching = {isFetching: false};

function useCoinDesk() {
	const [coinDeskData, setCoinDeskData] = useState({data: null});
	const [error, setError] = useState({error: null});

	useEffect(() => {
		if (!coinDeskData.data && !fetching.isFetching) {
			fetching.isFetching = true;

			fetch(COIN_DESK_API_URL)
				.then(async response => {
					const data = await response.json();
					setCoinDeskData({
						data
					});
				})
				.catch(setError)
				.finally(() => fetching.isFetching = false);
		}
	}, [coinDeskData.data, setCoinDeskData, setError]);

	return {
		data: coinDeskData?.data,
		error
	};
}

export default useCoinDesk;

export {
	useCoinDesk
};
