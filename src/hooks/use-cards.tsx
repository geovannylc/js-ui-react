import { CoinDesk } from "../types/coindesk.type";
import { Currency } from "../types/currency.type";
import { CardItem } from "../types/card-items.type";

function createCardItems(data: CoinDesk): CardItem[] {
	return Object.keys(data.bpi)
		.map((key: string) => {
			return {
				...data.bpi[key as Currency] ?? null,
				currency: key as Currency,
				disclaimer: data.disclaimer,
				updated_at: data.time.updated
			};
		});
}

function useCards() {
	return {
		createCardItems
	};
}

export default useCards;

export {
	useCards
};
