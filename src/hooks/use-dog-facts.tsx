import { useEffect, useState } from "react";
import { DOG_FACTS_API_URL } from "../constants";
import { DogFactsPaginated } from "../types/dog-facts-paginated.type";

const fetching = {isFetching: false};

function useDogFacts() {
	const [dogFactsData, setDogFactsData] = useState({} as DogFactsPaginated);
	const [error, setError] = useState({error: null});

	useEffect(() => {
		if (!dogFactsData.data && !fetching.isFetching) {
			fetching.isFetching = true;

			fetch(DOG_FACTS_API_URL)
				.then(async response => {
					const dataResponse = await response.json();

					setDogFactsData(dataResponse as DogFactsPaginated);
				})
				.catch(setError)
				.finally(() => fetching.isFetching = false);
		}
	}, [dogFactsData.data, setDogFactsData, setError]);

	return {
		data: dogFactsData?.data,
		error
	};
}

export default useDogFacts;

export {
	useDogFacts
};
