export default function displaySymbol(currency: string) {
	return currency.replace('&#36;', '$')
		.replace('&pound;', '£')
		.replace('&euro;', '€');
}
