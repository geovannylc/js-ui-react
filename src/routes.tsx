import { createBrowserRouter, } from "react-router-dom";
import { Dashboard } from "./components/";
import Bitcoin from './components/Bitcoin/Bitcoin';
import React from "react";

export const routes = createBrowserRouter([
	{
		path: "/",
		element: <Dashboard></Dashboard>
	},
	{
		path: "/bitcoin",
		element: <Bitcoin></Bitcoin>
	}
]);
