import { act, cleanup, render, screen } from "@testing-library/react";
import DogFact from "./DogFact";

describe("DogFact", () => {
	afterEach(cleanup);

	beforeEach(() => {
		const mockSetState = jest.fn();

		jest.mock('react', () => ({
			useState: (setDogFactsData: boolean) => [setDogFactsData, mockSetState]
		}));

		jest.spyOn(global, "fetch").mockImplementation(() => Promise.resolve({
			json: () => Promise.resolve({
				"current_page": 1,
				"data": [
					{
						"id": 75,
						"fact": "Some fact about dogs"
					},
					{
						"id": 76,
						"fact": "Some fact about dogs"
					},
					{
						"id": 77,
						"fact": "Some fact about dogs"
					},
					{
						"id": 78,
						"fact": "Some fact about dogs"
					}
				],
				"first_page_url": "http://localhost:8042/api/v1/dog-facts?page=1",
				"from": 1,
				"last_page": 1,
				"last_page_url": "http://localhost:8042/api/v1/dog-facts?page=1",
				"links": [
					{
						"url": null,
						"label": "&laquo; Previous",
						"active": false
					},
					{
						"url": "http://localhost:8042/api/v1/dog-facts?page=1",
						"label": "1",
						"active": true
					},
					{
						"url": null,
						"label": "Next &raquo;",
						"active": false
					}
				],
				"next_page_url": null,
				"path": "http://localhost:8042/api/v1/dog-facts",
				"per_page": 25,
				"prev_page_url": null,
				"to": 4,
				"total": 4
			})
		} as Response));
	});
	test("The title of the page should change from 'Dog Facts Dashboard'", async () => {
		render(<DogFact/>);

		await act(() => {
			const title = screen.getByText(/dog facts dashboard/i);
			expect(title).toBeInTheDocument();
		});

	});
});
