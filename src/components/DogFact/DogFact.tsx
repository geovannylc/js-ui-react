import { CustomTable, Title } from "../index";
import useDogFacts from "../../hooks/use-dog-facts";
import { CustomTableRow } from "../../types/custom-table-row.type";
import React, { useMemo } from "react";

export default function DogFact() {
	const {data} = useDogFacts();
	const rows: CustomTableRow[] = useMemo(() => {
		return data?.map((row, index) => ({
			align: "left",
			data: {
				id: row.id,
				fact: row.fact
			}
		})) || []
	}, [data]);

	const columns: string[] = [
		'id',
		'fact'
	];

	return (<div>
			<Title>Dog Facts Dashboard</Title>
			<CustomTable columns={columns} rows={rows}/>
		</div>
	);
}
