import { render, screen } from '@testing-library/react';
import Bitcoin from './Bitcoin';
import React from 'react';

const mockData = {
	time: {
		updated: "Oct 2, 2023 05:13:00 UTC",
		updatedISO: "2023-10-02T05:13:00+00:00",
		updateduk: "Oct 2, 2023 at 06:13 BST",
	},
	disclaimer:
		"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
	chartName: "Bitcoin",
	bpi: {
		USD: {
			code: "USD",
			symbol: "&#36;",
			rate: "28,056.5050",
			description: "United States Dollar",
			rate_float: 28056.505,
		},
		GBP: {
			code: "GBP",
			symbol: "&pound;",
			rate: "23,443.7911",
			description: "British Pound Sterling",
			rate_float: 23443.7911,
		},
		EUR: {
			code: "EUR",
			symbol: "&euro;",
			rate: "27,331.1321",
			description: "Euro",
			rate_float: 27331.1321,
		},
	},
};
test("The title of the page should change from 'Dog Facts Dashboard' to 'Bitcoin Currency Exchange'", () => {
	render(<Bitcoin data={mockData}/>);
	const title = screen.getByText(/bitcoin currency exchange/i);
	expect(title).toBeInTheDocument();
});
