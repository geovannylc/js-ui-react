/** @format */

import React from "react";
import "./Bitcoin.css";
import { Title } from "../";
import { CoinDesk } from "../../types/coindesk.type";
import { Grid } from "@mui/material";
import { BitcoinProps } from "../../types/bitcoin-props.type";
import OutlinedCard from "../OutlinedCard/OutlinedCard";
import useCards from "../../hooks/use-cards";
import Box from "@mui/material/Box";


export default function Bitcoin({data}: BitcoinProps) {
	const {createCardItems} = useCards();

	const cards = data ? createCardItems(data as CoinDesk) : [];

	return (
		<div>
			<Title>Bitcoin Currency Exchange</Title>
			<div className="Bitcoin">
				<Box
					sx={{
						minWidth: 800,
						justifyItems: 'center',
					}}
				>
					<Grid container spacing={{xs: 1, md: 3}}
								columns={{xs: 1, sm: 1, md: 12}}
								maxWidth={'lg'}
								justifyContent={'space-between'}>
						{cards && cards.map((card, index) => {
							return (
								<Grid item xs={1} md={3} lg={3} key={index}>
									<OutlinedCard
										key={card.currency}
										item={card}
									/>
								</Grid>
							);
						})}
					</Grid>
				</Box>

			</div>
		</div>
	);
}
