import { render, screen } from '@testing-library/react';
import { Deposits, Orders } from '../';
import React from 'react';

test('renders orders', () => {
  render(<Orders />);
  const title = screen.getByText(/recent orders/i);
  expect(title).toBeInTheDocument();
});

test('renders deposits', () => {
  render(<Deposits />);
  const title = screen.getByText(/recent deposits/i);
  expect(title).toBeInTheDocument();
});
