import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Link from "@mui/material/Link";
import Header from '../Header/Header';
import React from "react";
import { Route, Routes } from "react-router-dom";
import Dashboard from "../Dashboard/Dashboard";
import { Bitcoin } from "../";
import useCoinDesk from "../../hooks/use-coin-desk";

function Copyright(props: any) {
	return (
		<Typography
			variant="body2"
			color="text.secondary"
			align="center"
			{...props}
		>
			{"Copyright © "}
			<Link color="inherit" href="https://mui.com/">
				Your Website
			</Link>{" "}
			{new Date().getFullYear()}
			{"."}
		</Typography>
	);
}

function App() {
	const [open, setOpen] = React.useState(true);
	const {data} = useCoinDesk();

	const toggleDrawer = () => {
		setOpen(!open);
	};

	return (
		<Box sx={{display: "flex"}}>
			<Header open={open} toggleDrawer={toggleDrawer}/>
			<Box
				component="main"
				sx={{
					backgroundColor: (theme) =>
						theme.palette.mode === "light"
							? theme.palette.grey[100]
							: theme.palette.grey[900],
					flexGrow: 1,
					height: "100vh",
					overflow: "auto",
				}}
			>
				<Toolbar/>
				<Container maxWidth="lg" sx={{mt: 4, mb: 4}}>
					<Routes>
						<Route path="/">
							<Route index element={<Dashboard/>}/>
							<Route path="/bitcoin" element={<Bitcoin data={data}/>}/>
						</Route>
					</Routes>
					<Copyright sx={{pt: 4}}/>
				</Container>
			</Box>
		</Box>
	);
}

export default App;
