import { render } from "@testing-library/react";
import React from "react";
import OutlinedCard from "./OutlinedCard";
import { CardItem } from "../../types/card-items.type";

const date = (new Date()).toDateString();
const mockData = {
	code: "USD",
	symbol: "&#36;",
	rate: "28,056.5050",
	description: "United States Dollar",
	rate_float: 28056.505,
	currency: "USD",
	disclaimer: 'Testing',
	updated_at: date
} as CardItem;
test("Checking description", () => {
	const {getByLabelText, queryByLabelText, getByText} = render(<OutlinedCard
		item={mockData}/>);

	expect(getByText(mockData.disclaimer)).toBeInTheDocument();
	expect(getByText(mockData.description)).toBeInTheDocument();
	expect(getByText('$ ' + mockData.rate_float.toFixed(2))).toBeInTheDocument();
	expect(getByText(date)).toBeInTheDocument();
});
