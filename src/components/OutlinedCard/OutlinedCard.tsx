import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardItem } from "../../types/card-items.type";
import displaySymbol from "../../helpers/display-symbol";

type CardProps = {
	item: CardItem;
};

export default function OutlinedCard({item}: CardProps) {
	return (
		<React.Fragment>
			<Box sx={{
				width: 300,
				height: 300,
			}}>
				<Card variant="outlined">
					<CardContent>
						<Typography sx={{fontSize: 14}} color="text.secondary" gutterBottom>
							{item.updated_at}
						</Typography>
						<Typography variant="h5" component="div">
							{item.description}
						</Typography>
						<Typography variant="body2">
							{displaySymbol(item.symbol) + ' ' + item.rate_float.toFixed(2)}
						</Typography>
					</CardContent>
					<CardActions>
						<Typography sx={{fontSize: 12}} color="text.secondary">
							{item.disclaimer}
						</Typography>
					</CardActions>
				</Card>
			</Box>
		</React.Fragment>
	);
}
