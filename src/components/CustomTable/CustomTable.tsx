import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { CustomTablePros } from "../../types/custom-table-props.type";

export default function CustomTable({
																			columns,
																			rows
																		}: CustomTablePros) {
	return (
		<TableContainer component={Paper}>
			<Table sx={{minWidth: 650}} aria-label="simple table">
				<TableHead>
					<TableRow key={'table-row-custom-table'}>
						{columns.map((column, index) => {
							return (
								<TableCell key={index}>{column}</TableCell>
							);
						})}
					</TableRow>
				</TableHead>
				<TableBody>
					{rows && rows.map((row, index) => {
						return (
							<TableRow
								key={index}
								sx={{'&:last-child td, &:last-child th': {border: 0}}}
							>
								{columns && columns.map((column, index) => {
									return (
										<TableCell component="th" scope="row"
															 key={'table-cell-' + index}>
											{row.data[column]}
										</TableCell>
									);
								})}

							</TableRow>
						);
					})}
				</TableBody>
			</Table>
		</TableContainer>
	);
}
