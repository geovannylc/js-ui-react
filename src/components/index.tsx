import Deposits from './Deposits/Deposits';
import Chart from './Chart/Chart';
import Dashboard from './Dashboard/Dashboard';
import Orders from './Orders/Orders';
import Title from './Title/Title';
import App from './App/App';
import Bitcoin from './Bitcoin/Bitcoin';
import OutlinedCard from './OutlinedCard/OutlinedCard';
import CustomTable from "./CustomTable/CustomTable";

export {
	App,
	Deposits,
	Chart,
	Dashboard,
	Orders,
	Title,
	Bitcoin,
	OutlinedCard,
	CustomTable
};
