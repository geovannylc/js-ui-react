import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import DogFact from "../DogFact/DogFact";

export default function Dashboard() {
	return (<Grid container spacing={3}>
		<Grid item xs={12}>
			<Paper sx={{p: 2, display: "flex", flexDirection: "column"}}>
				<DogFact/>
			</Paper>
		</Grid>
	</Grid>);
}

