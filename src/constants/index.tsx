/** @format */

import * as React from "react";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import DashboardIcon from "@mui/icons-material/Dashboard";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PeopleIcon from "@mui/icons-material/People";
import BarChartIcon from "@mui/icons-material/BarChart";
import LayersIcon from "@mui/icons-material/Layers";
import AssignmentIcon from "@mui/icons-material/Assignment";
import { Link } from "react-router-dom";
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';

export const mainListItems = (
	<React.Fragment>
		<Link to={"/bitcoin"}>
			<ListItemButton>
				<ListItemIcon>
					<AttachMoneyIcon/>
				</ListItemIcon>
				<ListItemText primary="Bitcoin"/>
			</ListItemButton>
		</Link>
		<Link to={"/"}>
			<ListItemButton>
				<ListItemIcon>
					<DashboardIcon/>
				</ListItemIcon>
				<ListItemText primary="Dashboard"/>
			</ListItemButton>
		</Link>
		<ListItemButton>
			<ListItemIcon>
				<ShoppingCartIcon/>
			</ListItemIcon>
			<ListItemText primary="Orders"/>
		</ListItemButton>
		<ListItemButton>
			<ListItemIcon>
				<PeopleIcon/>
			</ListItemIcon>
			<ListItemText primary="Customers"/>
		</ListItemButton>
		<ListItemButton>
			<ListItemIcon>
				<BarChartIcon/>
			</ListItemIcon>
			<ListItemText primary="Reports"/>
		</ListItemButton>
		<ListItemButton>
			<ListItemIcon>
				<LayersIcon/>
			</ListItemIcon>
			<ListItemText primary="Integrations"/>
		</ListItemButton>
	</React.Fragment>
);

export const secondaryListItems = (
	<React.Fragment>
		<ListSubheader component="div" inset>
			Saved reports
		</ListSubheader>
		<ListItemButton>
			<ListItemIcon>
				<AssignmentIcon/>
			</ListItemIcon>
			<ListItemText primary="Current month"/>
		</ListItemButton>
		<ListItemButton>
			<ListItemIcon>
				<AssignmentIcon/>
			</ListItemIcon>
			<ListItemText primary="Last quarter"/>
		</ListItemButton>
		<ListItemButton>
			<ListItemIcon>
				<AssignmentIcon/>
			</ListItemIcon>
			<ListItemText primary="Year-end sale"/>
		</ListItemButton>
	</React.Fragment>
);

export const COIN_DESK_API_URL = "https://api.coindesk.com/v1/bpi/currentprice.json";
export const DOG_FACTS_API_URL = "http://localhost:8042/api/v1/dog-facts";
